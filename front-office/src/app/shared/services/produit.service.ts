import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Produit} from '../models/Produit';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  httpOptions={
    headers: new HttpHeaders({'Content-Type':'application/json'})
  }
  data:Observable <any>;
  produitsUrl: string='http://localhost:8080/formation/public/api/products';
  ajoutUrl:string ='http://localhost:8080/formation/public/api/product';
  deleteUrl:string='http://localhost:8080/formation/public/api/product';
  constructor(private http: HttpClient) {}

  // saveProduit(produit: Produit):Observable<Produit>{
  //   return this.http.post<Produit>(this.ajoutUrl,produit,this.httpOptions);
  // }
  getProduits():Observable<Produit[]>{
    return this.http.get<Produit[]>(this.produitsUrl);
  }
  // deleteProduct(produit: Produit |number):Observable<Produit>{
  //   const id =typeof produit =='number'? produit: produit.id;
  //   const url=`${this.deleteUrl}/${id}`;
  //   return this.http.delete<Produit>(url,this.httpOptions);
  // }

} 




