import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ProduitService} from './shared/services/produit.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { FullLayoutComponent } from './full-layout/full-layout.component';
// import { HeaderComponent } from './full-layout/header/header.component';
// import { FooterComponent } from './full-layout/footer/footer.component';
// import { BodyComponent } from './full-layout/body/body.component';
// import { LoginComponent } from './compte/login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { FullLayoutModule } from './full-layout/full-layout.module';
import { UserModule } from './user/user.module';
import { FullLayoutComponent } from './full-layout/full-layout.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    CommonModule,
    FullLayoutModule,
    UserModule
  ],
  providers: [ProduitService],
  bootstrap: [AppComponent]
})
export class AppModule { }

// FullLayoutComponent,
//     HeaderComponent,
//     FooterComponent,
//     BodyComponent,
//     LoginComponent,
// RegisterComponent,