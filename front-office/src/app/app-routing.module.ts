import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { FullLayoutComponent } from './full-layout/full-layout.component';
import { UserModule } from './user/user.module';
import { FullLayoutModule } from './full-layout/full-layout.module';

// export function loadFullLayoutModule() {
//   console.log("loadFullLayoutModule .....");
//   return FullLayoutComponent;
// }
export function loadUserModule() {
  console.log("loadCompteModule .....");
  return UserModule;
}
const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    children: [
      {
        path: 'user',
        loadChildren: loadUserModule
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// ,
//         children: [
//           {
//             path: 'login',
//             component: LoginComponent
//           },
//           {
//             path: 'register',
//             component: RegisterComponent
//           }
//         ]