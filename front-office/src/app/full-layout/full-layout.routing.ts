import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FullLayoutComponent} from './full-layout.component';


export const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FullLayoutRouting {
}
