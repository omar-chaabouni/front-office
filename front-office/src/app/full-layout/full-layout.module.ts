import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FullLayoutComponent} from './full-layout.component';
import { FullLayoutRouting } from './full-layout.routing';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [FullLayoutComponent,HeaderComponent,BodyComponent,FooterComponent],
  imports: [
    CommonModule,
    FullLayoutRouting
  ]
})
export class FullLayoutModule { }
