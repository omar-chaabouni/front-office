import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor() { }
  
  ngOnInit() {  
    (($) => {
      $('#language').hover(() => {
      $('#language>ul').css('display', 'block');
      });
      })(jQuery);
      
    (($) => {
      $('#language').mouseleave(() => {
      $('#language>ul').css('display', 'none');
      });
      })(jQuery);
      
      (($) => {
        $('#currency').hover(() => {
        $('#currency>ul').css('display', 'block');
        });
        })(jQuery);
        
      (($) => {
        $('#currency').mouseleave(() => {
        $('#currency>ul').css('display', 'none');
        });
        })(jQuery);
        
        // (($) => {
        //   $('#menu').hover(() => {
        //   $('#menu > ul > li.categories_hor> div ').css('display', 'block');
        //   });
        //   })(jQuery);
          
        // (($) => {
        //   $('#menu').mouseleave(() => {
        //   $('#menu > ul > li.categories_hor> div ').css('display', 'none');
        //   });
        //   })(jQuery);
          

        //   (($) => {
        //     $('#menu').hover(() => {
        //     $('#menu > ul > li.omar ').css('display', 'block');
        //     });
        //     })(jQuery);
            
        //   (($) => {
        //     $('#menu').mouseleave(() => {
        //     $('#menu > ul > li.omar ').css('display', 'none');
        //     });
        //     })(jQuery);

    }
  }