import { Component, OnInit } from '@angular/core';
import {Produit} from '../../shared/models/Produit';
import {ProduitService} from '../../shared/services/produit.service';
@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  produit : Produit = new Produit();
  produits:Produit[];
  pro_len:number;
  constructor(private service:ProduitService) { }

  ngOnInit() {this.service.getProduits().subscribe
    (prod=>{this.produits=prod;
    this.pro_len=this.produits.length;});
  }

}
