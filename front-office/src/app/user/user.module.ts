import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { CompteRouting } from './user.routing';

@NgModule({
  declarations: [RegisterComponent, LoginComponent],
  imports: [
    CompteRouting,
    CommonModule,
    SharedModule
  ]
})
export class UserModule {
 }
