import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    (($) => {
      $('#language').hover(() => {
      $('#language>ul').css('display', 'block');
      });
      })(jQuery);
      
    (($) => {
      $('#language').mouseleave(() => {
      $('#language>ul').css('display', 'none');
      });
      })(jQuery);
      
      (($) => {
        $('#currency').hover(() => {
        $('#currency>ul').css('display', 'block');
        });
        })(jQuery);
        
      (($) => {
        $('#currency').mouseleave(() => {
        $('#currency>ul').css('display', 'none');
        });
        })(jQuery);
  }

}
