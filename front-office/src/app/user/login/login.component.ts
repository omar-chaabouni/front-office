import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    (($) => {
      $('#language').hover(() => {
      $('#language>ul').css('display', 'block');
      });
      })(jQuery);
      
    (($) => {
      $('#language').mouseleave(() => {
      $('#language>ul').css('display', 'none');
      });
      })(jQuery);
      
      (($) => {
        $('#currency').hover(() => {
        $('#currency>ul').css('display', 'block');
        });
        })(jQuery);
        
      (($) => {
        $('#currency').mouseleave(() => {
        $('#currency>ul').css('display', 'none');
        });
        })(jQuery);
  }

}
